// Looping While
console.log("-------------SOAL WHILE --------------")
console.log("LOOPING PERTAMA")
var i = 2;
while (i<=20) {
    console.log(i + "- I Love Coding");
    i+=2;
}

console.log("LOOPING KEDUA")
var j = 20;
while (j>=2) {
    console.log(j + "- I Will Become a Mobile Developer");
    j-=2;
}

// Looping FOr
var santai = " - Santai";
var berkualitas = " - Berkualitas";
var lovecoding = " -I Love Coding";

console.log("-------------SOAL FOR--------------")
for (i=1 ; i<=20; i++){
    if(i % 2 != 1){
        console.log(i + berkualitas);
    } else if (i % 3 == 0){
        console.log(i + lovecoding);
    } else {
        console.log(i + santai);
    }
}

//  Looping Persegi Panjang dengan #
console.log("-------------SOAL Buat Persegi Pnajnga--------------")
var i = 1;
var j = 1;
var p = 8;
var l =4;
var pagar = " ";

while (j <= l) {
    while (i <= p) {
        pagar += "#";
        i++;
    }
    console.log(pagar);
    pagar= ' ' ;
    i=1;
    j++;
}

//  Looping membuat tangga
console.log("-------------SOAL Buat Tangga 7x7--------------")
i =1;
j=1;
var a = 7;
var t = 7;
var pagar = "";

for (i=1; i<= t; i++){
    for (j=1; j<=i; j++){
        pagar +="#";
    }

    console.log(pagar);
    pagar="";
}

//  Looping Membuat papan catur
console.log("-------------SOAL Buat Papan catur 8x8--------------")
i=1;
j=1;
var p = 8;
var l = 8;
var papan = "";

for (j = 1; j<=l; j++){
    if(j%2==1) {
        for(i=1; i<=p; i++){
            if(i% 2==1){
                papan +=" "
            } else {
                papan +="#"
            }
        }
    } else {
        for (i=1; i<=p; i++){
            if(i%2 ==1){
                papan +="#"
            } else {
                papan += " "
            }
        }
    }
    console.log(papan);
    papan = "";
}